<?php

global $baseArticlePath;

$baseArticlePath = 'articles/';

echo "<head>
<link rel='stylesheet' href='http://design.wikimedia.org/style-guide/css/build/wmui-style-guide.min.css'>
<link rel='stylesheet' href='main.css'>
<script src='http://code.jquery.com/jquery-3.6.0.min.js'></script>
<script src='main.js'></script>
</head>";

$title = '';
$body = '';
if (isset( $_GET['title'] ) ) {
    $title = $_GET['title'];
    $body = file_get_contents( sprintf('%s/%s', $baseArticlePath, $title ) );
}

$wordCount = getWordCount();
echo "<body>";
echo "<header id=header class=header><div class='content-box'><a href='/'><h1 class='site__title'>WikiPHPedia</h1></a><div>$wordCount</div></div></header>";
echo "<div class=page>";
echo "<div class=content-box>";
echo "<div class='col'>";
 echo "<form action='index.php' method='post'>
<input name='title' type='text' placeholder='Article title...' value=$title>
<br />
<textarea name='body' placeholder='Article body...' >$body</textarea>
<br />
<input type='submit' name='submit' value='Submit' />
<br />
</div>
<div class='col'>
<h2>Preview</h2>
$title\n\n
$body
</div>
<h2>Articles</h2>
<ul>
<li><a href='index.php?title=Foo'>Foo</a></li>
</ul>
</form>";

if ( $_POST ) {
	doEdit( $_POST['title'], $_POST['body'] );
}
echo "</div>";
echo "</div>";
echo "</body";

function doEdit( $title, $body ) {
	global $baseArticlePath;
	error_log( "Saving article $title, success!" );
	file_put_contents( sprintf("%s/%s", $baseArticlePath, $title ), $body );
}

function getWordCount() {
	global $baseArticlePath;
	$wc = 0;
	$dir = new DirectoryIterator($baseArticlePath);
	foreach ($dir as $fileinfo) {
		if ( $fileinfo->isDot() ) {
			continue;
		}
		$c = file_get_contents( $baseArticlePath . $fileinfo->getFilename() );
		$ch = explode( " ", $c );
		$wc += count($ch);
	}
	return "$wc words written";
}
